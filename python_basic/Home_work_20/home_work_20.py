import csv
import openpyxl


def create_data() -> list:
    data = []
    with open('../Home_work_19/data.csv', encoding='utf-8') as file:
        file_reader = csv.reader(file)
        for idx, row in enumerate(file_reader):
            data.append(row)
            del data[idx][2]  # прибираємо вік
    # розгортаємо таблицю
    new_data = []
    for i in range(len(data[0])):
        temp_data = []
        for j in range(len(data)):
            temp_data.append(data[j][i])
        new_data.append(temp_data)
    return new_data


def main():
    data = create_data()
    wb = openpyxl.Workbook()
    wb.create_sheet('Список юзерів', index=0)
    sheet = wb['Список юзерів']
    for row_idx, row in enumerate(data):
        for col_idx, value in enumerate(row):
            cell = sheet.cell(row=row_idx + 1, column=col_idx + 1)
            cell.value = value
    wb.save('home_work.xlsx')


if __name__ == '__main__':
    main()
