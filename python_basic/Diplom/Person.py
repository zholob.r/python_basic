import re
from datetime import datetime


class Person:
    def __init__(self):
        self.name = ''
        self.date_of_birth = None
        self.date_of_death = None
        self.sex = ''
        self.person_info = ''

    def add_persone(self):
        # ПІБ не обов'язкково може починається з великої  я приведу в великий регістр
        # https://ru.wikipedia.org/wiki/Кириллица_(блок_Юникода)#Список_символов
        pattern_name = r"^[A-Za-zА-Ща-щ`'їЇІієЄґҐьюЮяЯ]+$"
        print('Додавання нового користувача до бази (ім\'я може вводитись українською чи англійською):')
        self.name = Person.add_valid_name(pattern_name)
        self.name += Person.add_valid_surname(pattern_name, 'прізвище')
        self.name += Person.add_valid_surname(pattern_name, 'по батькові')
        self.date_of_birth = Person.add_valid_date('народження')
        self.date_of_death = Person.add_valid_date('смерті')
        self.sex = Person.add_valid_sex()
        self.add_persone_info()

    @staticmethod
    def add_valid_name(pattern):
        while True:
            name = input('Введіть ім\'я (обов\'язково) >>> ').strip()
            if re.match(pattern, name):
                break
            else:
                print('Ім\'я може складатись лиже з українських літер та апострофу ')
        return name.capitalize()

    @staticmethod
    def add_valid_surname(pattern, what_name: str):
        while True:
            surname = input(f'Введіть {what_name} (не обов\'язково) >>> ').strip()
            if not surname:
                break
            elif re.match(pattern, surname):
                surname = surname
                break
            else:
                print(f'{what_name.title()} може складатись лиже з літер та апострофу ')
        return ' ' + surname.capitalize()

    @staticmethod
    def add_valid_date(what_date: str):
        version_date = ['%d.%m.%Y', '%d %m %Y', '%d/%m/%Y', '%d-%m-%Y']
        comment_death = '(може бути відсутня)' if what_date == 'смерті' else ''
        while True:
            person_date = input(f'Введіть дату {what_date} в форматі число місяць рік {comment_death} >>> ').strip()
            # дата смерті не обов'язкова і якщо передається порожня строка то завершуємо
            if what_date == 'смерті' and not person_date:
                break
            for item_date in version_date:
                try:
                    right_date = datetime.strptime(person_date, item_date)
                    break
                except ValueError:
                    continue
            else:
                print(f'Введіть, будь-ласка корректну дату {what_date}, наприклад: '
                      f'12.10.1980 | 11 10 2000 | 01/02/1995 | 3-9-2007')
                continue
            right_date = right_date.strftime('%d.%m.%Y')
            return right_date

    @staticmethod
    def add_valid_sex():
        while True:
            sex = input(f'Введіть стать (m / f або ч / ж) >>> ').lower().strip()
            if sex in ('m', 'f', 'ч', 'ж'):
                break
            else:
                print('Помилка вводу!')
        return 'm' if sex in ('m', 'ч') else 'f'

    def add_persone_info(self):
        self.person_info = f'{self.name}, {self.date_of_birth}, '
        self.person_info += f'{self.date_of_death}, ' if self.date_of_death else ''
        self.person_info += self.sex
