from datetime import datetime
import os
import openpyxl
from Person import Person
import re


class People:
    def __init__(self):
        self.people_data_list = []  # list об'єктів Persone
        self.people_info_list = []  # list строк з інфою про людей
        # Євген Крут Михайлович, 12.10.1980, 11.10.2001, m
        # = Євген Крут Михайлович 20 років, чоловік. Народився 12.10.1980. Помер: 11.10.2001.

    def add_people(self, person):
        self.people_data_list.append(person)
        self.people_info_list.append(People.add_people_info(person))

    def show(self):
        if not self.people_info_list:
            print('База порожня, нам нема що виводити!')
        else:
            print('Список людей в базі:')
            for people in self.people_info_list:
                print(people)

    def find_users(self):
        if not self.people_data_list:
            print('База порожня, нам нема що шукати!')
        else:
            print('Пошук може здійснюватися по імені, прізвищу та по батькові')
            what_find = input('Що шукаємо ? >>> ').lower().strip()
            flag_some_find = False

            for idx, item in enumerate(self.people_data_list):
                if item.name.lower().find(what_find) >= 0:
                    print(self.people_info_list[idx])
                    flag_some_find = True

            if not flag_some_find:
                print(f'За Вашим пошуком {what_find} нічого не знайдено!')

    def save_in_file(self):
        if not self.people_data_list:
            print('База порожня, нам нема що шукати!')
        else:
            current_dir = os.path.dirname(os.path.abspath(__file__))
            files = os.listdir(current_dir)
            file_name = ''
            xl_file = list(filter(lambda x: x.find('.xlsx') != -1, files))
            print('0 - зберегти базу в новий файл')
            if xl_file:
                for idx, file in enumerate(xl_file):
                    print(f'{idx + 1} - зберегти в {file}')
            while True:
                try:
                    save_choice = int(input('Оберіть куди зберігаємо базу >>> '))
                    if save_choice != 0:
                        file_name = xl_file[save_choice - 1]
                    break
                except ValueError:
                    print('Ви ввели не число!')
                except IndexError:
                    print('Ви обрали варіант якого не існує!')
                except Exception:
                    print('Ви щось ввели не так, спробуйте заново!')
            # якщо збереження в новий файл
            if not file_name:
                while True:
                    # може містити будь-які символи (крім /,\,*,?,:,<,>,|), кількість символів <= 255
                    file_name = input('Введіть назву файлу >>> ')
                    pattern = r'^[^/\\*?:<>|]+$'
                    if not re.match(pattern, file_name):
                        print('Ім\'я файлу не може містити символи /,\,*,?,:,<,>,|')
                    elif len(file_name) > 255:
                        print('Ім\'я файлу не може бути більше 255 символів!')
                    else:
                        break
            # на всяк переконаємось що користувач не ввів розширення і самі його додамо
            file_name.replace('.xlsx', '')
            file_name += '.xlsx'

            wb = openpyxl.Workbook()
            sheet = wb['Sheet']

            for row in range(len(self.people_data_list)):
                sheet.cell(row=row + 1, column=1).value = self.people_data_list[row].name
                sheet.cell(row=row + 1, column=2).value = self.people_data_list[row].date_of_birth
                sheet.cell(row=row + 1, column=3).value = self.people_data_list[row].date_of_death
                sheet.cell(row=row + 1, column=4).value = self.people_data_list[row].sex

            wb.save(file_name)
            print(f'Базу з {len(self.people_data_list)} записів успішно збрежено у файл {file_name}')

    def load_from_file(self):
        # спершу тре глянути чи є що завантажувати
        current_dir = os.path.dirname(os.path.abspath(__file__))
        files = os.listdir(current_dir)

        xl_file = list(filter(lambda x: x.find('.xlsx') != -1, files))
        if not xl_file:
            print('В нас нема фалів з базою!')
        else:
            print('Є наступні файли з базою:')
            for idx, file in enumerate(xl_file):
                print(f'{idx + 1} - {file}')

        while True:
            try:
                number_file = int(input('Оберіть з якого фалу підтягуємо базу ввевши номер файлу >>> '))
                file_name = xl_file[number_file - 1]
                break
            except ValueError:
                print('Ви ввели не число!')
            except IndexError:
                print('Ви ввели номер файлу якого не існує!')
            except Exception:
                print('Ви щось ввели не так, спробуйте заново!')

        wb = openpyxl.load_workbook(file_name)
        sheet = wb.active
        rows = sheet.max_row

        for i in range(1, rows + 1):
            person = Person()
            person.name = sheet.cell(row=i, column=1).value
            # забавна особливість що якщо ми імпортуємо з .xlsx дату то вона зберігається НЕ як str
            # а як <class 'datetime.datetime'> і 10.03.2000  = 2000-03-10 00:00:00
            person.date_of_birth = sheet.cell(row=i, column=2).value.strftime('%d.%m.%Y')
            date_of_death = sheet.cell(row=i, column=3).value
            person.date_of_death = date_of_death.strftime('%d.%m.%Y') if date_of_death else None
            person.sex = sheet.cell(row=i, column=4).value
            person.add_persone_info()
            self.add_people(person)
        print(f'В базу було успішно додано {rows} записи')

    @staticmethod
    def add_people_info(person):
        people_age = People.calculate_age(person)
        people_info = f'{person.name} {people_age}, '
        people_info += 'чоловік. ' if person.sex in ('m', 'ч') else 'жінка. '
        people_info += People.add_age(person)

        return people_info

    @staticmethod
    def calculate_age(person):

        birth_time = datetime.date(datetime.strptime(person.date_of_birth, '%d.%m.%Y'))
        death_time = datetime.date(
            datetime.strptime(person.date_of_death, '%d.%m.%Y')) if person.date_of_death else datetime.date(
            datetime.now())

        delta_day = (death_time - birth_time).days
        year_user = -1  # бо далі буду ітерувати в циклі і коли пройде менше 1 року то буде вже 0 років

        for year in range(birth_time.year, death_time.year + 1):
            if delta_day >= 0:
                delta_day -= 366 if year % 4 == 0 else 365
                year_user += 1

        # вже вияснили скільки користувачу років year_user, залишилось вияснити приставку рік, років, роки
        age_str = ''
        end_year_user = year_user % 10
        if end_year_user == 0 or 5 <= end_year_user <= 9 or year_user in (11, 12, 13, 14):
            age_str = 'років'
        elif end_year_user == 1:
            age_str = 'рік'
        else:
            age_str = 'роки'

        if year_user == -1:
            year_user = ''
            age_str = 'Зверніть увагу, користувач ще не народився, або помер раніше ніж народився'

        return f'{year_user} {age_str}'

    @staticmethod
    def add_age(person):
        str_result = 'Народився ' if person.sex == 'm' else 'Народилася '

        str_result += f'{person.date_of_birth}. '
        if person.date_of_death:
            str_result += 'Помер ' if person.sex == 'm' else 'Померла '
            str_result += f'{person.date_of_death}.'

        return str_result
