import os
from People import People
from Person import Person


def main_menu(people):
    help_command = '''Команди: 
add / додати  - додати до бази людину              
find / пошук - пошук за вже введеними данними          
list / показати - показати данні про людей в базі  
save / зберегти - зберегти базу в файл     
lоad / завантажити  - вигрузити з файлу базу   
help / допомога  - вивести перелік команд     
exit / вихід - вийти з програми 
    '''
    short_command = '''\nadd / додати  - додати до бази людину              
find / пошук - пошук за вже введеними данними          
list / показати - показати данні про людей в базі  
save / зберегти - зберегти базу в файл     
load / завантажити  - вигрузити з файлу базу   
'''
    print(help_command)
    while True:
        user_command = input('Що бажаєте зробити? >>> ').strip().lower()
        if user_command in ('exit', 'вихід'):
            break
        elif user_command in ('help', 'допомога'):
            print(help_command)
        elif user_command.lower() in ('add', 'додати'):
            add_user_menu(people)
            print(short_command)
        elif user_command in ('list', 'показати'):
            show_user(people)
            print(short_command)
        elif user_command in ('find', 'пошук'):
            find_users(people)
            print(short_command)
        elif user_command in ('save', 'зберегти'):
            save_in_file(people)
            print(short_command)
        elif user_command in ('load', 'завантажити'):
            load_from_file(people)
            print(short_command)
        else:
            print(
                'Незрозуміла команда, Ви можете скораститсь командою help / допомога  щоб передивитись перелік команд')


def add_user_menu(people):
    os.system('cls')
    person = Person()
    person.add_persone()
    people.add_people(person)
    print(f'Дякую! Наступна запис була додана до бази: {person.person_info}')


def show_user(people):
    os.system('cls')
    people.show()


def find_users(people):
    os.system('cls')
    people.find_users()


def save_in_file(people):
    os.system('cls')
    people.save_in_file()


def load_from_file(people):
    os.system('cls')
    people.load_from_file()


def main():
    print("\tВітаю!\U0001F44B \n"
          "Програма зберігає дані про людину \U0001F60E, а саме:\n"
          "ПІБ, дата народження, дата смерті (може бути відсутня) і стать\n"
          "Для виходу введіть exit або вихід")

    people = People()
    main_menu(people)


if __name__ == '__main__':
    main()
