import random
import json


def create_dict() -> dict:
    # name = ('Roman', 'Victor', 'Anastasiya', 'Evgen', 'Inna', 'Maksym', 'Oleksandr', 'Dar`ya')
    name = ('Роман', 'Віктор', 'Анастасія', 'Євген', 'Інна', 'Максим')
    data = []
# генеруємо список в форматі [випадкове 6-ти значне число, (ім'я з кортежу name, випадковий вік з 10 до 35 ) ]
    for i in range(6):
        data.append([random.randrange(100000, 1000000), (name[i], random.randrange(10, 35))])
    return dict(data)


def main():
    data = create_dict()
    with open('data.json', 'w', encoding='utf-8') as file:
 # без ensure_ascii=False в .json кирилиця записується в кодіровці ascii
        json.dump(data, file, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    main()
