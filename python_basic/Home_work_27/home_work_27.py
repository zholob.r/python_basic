def my_gprogression_generator(start=1, step=2, end=6):
    while end:
        yield start
        start *= step
        end -= 1


def main():
    print('Перші 6 елементів геометричної прогресії -2 з кроком -5:')
    for item in my_gprogression_generator(-2, -5):
        print(item)
    print('\nПерші 5 елементів геометричної прогресії 10 з кроком 3:')
    for item in my_gprogression_generator(10, 3):
        print(item)


if __name__ == '__main__':
    main()
