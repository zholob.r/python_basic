import random

l_function = lambda x: 'не парне' if x % 2 == 1 else 'це нуль' if x == 0 else 'парне'


def main():
    number = random.randrange(0, 100)
    print(f'Число {number} {l_function(number)}')


if __name__ == '__main__':
    main()
