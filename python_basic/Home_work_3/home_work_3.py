user_input = input('Введіть речення з двох слів >>> ')

first_word, second_word = user_input.split()

s_string = '!%s %s?' % (second_word.upper(), first_word.title())
format_string = '!{} {}?'.format(second_word.upper(), first_word.title())
f_string = f'!{second_word.upper()} {first_word.title()}?'

# обов'язково використовуємо encoding='utf-8' якщо користувач буде вводити текст українською (кирилецею)
# без цього буде ломатись програма і працювати лише з латинецею !
my_file = open('home_work_3_out.txt', 'w', encoding='utf-8')
print(user_input, s_string, format_string, f_string, sep='<<<>>>', file=my_file)
my_file.close()
