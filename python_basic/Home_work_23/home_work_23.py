import math
from point import Point


class Circle(Point):

    def __init__(self, x=0, y=0, radius=1):
        super().__init__(x, y)
        self.radius = radius

    def area(self):
        return math.pi * (self.radius ** 2)

    def circumference(self):
        return 2 * math.pi * self.radius

    def __str__(self):
        return f'Circle(x={self.x}, y={self.y}, radius={self.radius})'

    def __add__(self, other):
        new_obj = super().__add__(other)
        return Circle(new_obj.x, new_obj.y, self.radius + other.radius)

    def __sub__(self, other):
        new_x = self.x - other.x
        new_y = self.y - other.y
        if self.radius == other.radius:
            return Point(new_x, new_y)

        return Circle(new_x, new_y, abs(self.radius - other.radius))


if __name__ == '__main__':
    first_c = Circle(1, 1, 5)
    second_c = Circle(1, 1, 5)
    third_c = Circle(2, 2, 7)
    print(first_c - second_c)
    print(first_c - third_c)
