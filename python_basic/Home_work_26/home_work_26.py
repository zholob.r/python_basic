import re


class MyExeptionNegativeExponentiation(Exception):
    def __init__(self, message='Не можна зводити в негативний ступінь!'):
        super().__init__(message)


class MyExeptionNegativeRoot(Exception):
    def __init__(self, message='Не можна вилучати негативний корінь!'):
        super().__init__(message)


class MyCalculator:
    def __init__(self):
        self.x = None
        self.y = None
        self.operator = ''
        self.result = None
        self.error = ''

    def calculate(self, expression) -> int | float:
        self.error = ''
        result = 0
        can_calculated = self.is_valid_data(expression)
        if can_calculated:
            if self.operator == '+':
                self.result = self.add(self.x, self.y)
            if self.operator == '-':
                self.result = self.sub(self.x, self.y)
            if self.operator == '*':
                self.result = self.mult(self.x, self.y)
            if self.operator == '/':
                self.result = self.div(self.x, self.y)
            if self.operator == '**':
                try:
                    self.result = self.exp(self.x, self.y)
                except MyExeptionNegativeExponentiation:
                    self.error = 'Не можна зводити в негативний ступінь!'
            if self.operator == '#':
                try:
                    self.result = self.root_ex(self.x, self.y)
                except MyExeptionNegativeRoot:
                    self.error = 'Не можна вилучати негативний корінь!'
        if self.error:
            print(self.error)
        else:
            if self.result.is_integer():
                self.result = int(self.result)
            print(f' = {self.result}')
        return result

    def is_valid_data(self, data: str) -> bool:
        """ логіка наступна: мені байдуже що введе користувач, головне знайти аріфметичний оператор (+, -, /, *, ** , #)
        далі все що зліва оператороа ввжаємо числом_1 а зправа числом_2
        програма буде видавати неправильний ввід в моменті спробі привести число_1 та число_2 до флоат
        """
        data = data.replace(',', '.')

        # (+, *, і #) мають спеціальне значення в регулярних виразах, тому їх потрібно екранувати за допомогою зворотного слеша (\)
        pattern = r'[\+\-/*\*\#]'
        what_return = False
        # знаходимо кінець 1го числа якщо таке є
        end_first_number = 0
        try:
            for idx, item in enumerate(data):
                if item in '1234567890.' and data[idx + 1] not in '1234567890.':
                    end_first_number = idx
                    break
            # symbol - список всіх операторів в строці
            # шукаємо наш оператор з кінця числа якщо воно є !
        except IndexError:  # якщо користувач введе лише числа
            self.error = 'Неправильний вираз!'

        symbol = re.findall(pattern, data[end_first_number:])

        operator = '**' if ''.join(symbol).find('**') != -1 else ''  # якщо взведення в ступінь, то одразу це зафіксуємо
        if len(symbol) >= 1 and not operator:
            operator = symbol[0]
        pos_operator = data.find(operator, end_first_number)
        str_x = data[:end_first_number + 1]
        str_y = data[pos_operator + len(operator):]

        if str_x and str_y:  # якщо в x та у точно щось заходить
            x, y = -1, -1
            try:
                x = float(str_x)
                y = float(str_y)
            except ValueError:  # якщо ввели не цифри
                self.error += 'Неправильний вираз!'
            except Exception:  # якщо незрозуміла помилка на всяк випадок
                self.error = 'Невідома помилка, напишить нам що Ви робили на пошту!'

            self.operator = operator
            self.x = x
            self.y = y
            what_return = True
        else:
            self.error = 'Неправильний вираз!'
        return what_return

    def add(self, x, y):
        result = None
        try:
            result = x + y
        except Exception:
            self.error = 'Невідома помилка, напишить нам що Ви робили на пошту!'
        return result

    def sub(self, x, y):
        result = None
        try:
            result = x - y
        except Exception:
            self.error = 'Невідома помилка, напишить нам що Ви робили на пошту!'
        return result

    def mult(self, x, y):
        result = None
        try:
            result = x * y
        except Exception:
            self.error = 'Невідома помилка, напишить нам що Ви робили на пошту!'
        return result

    def div(self, x, y):
        result = None
        try:
            result = x / y
        except ZeroDivisionError:
            self.error = 'Не можна ділити на 0 !'
        except Exception:  # якщо незрозуміла помилка на всяк випадок
            self.error = 'Невідома помилка, напишить нам що Ви робили на пошту!'
        return result

    def exp(self, x, y):
        result = None
        if y < 0:
            raise MyExeptionNegativeExponentiation()
        try:
            result = x ** y
        except Exception:
            self.error = 'Невідома помилка, напишить нам що Ви робили на пошту!'
        return result

    def root_ex(self, x, y):
        if y < 0:
            raise MyExeptionNegativeRoot()
        try:
            result = x ** (1 / y)
        except Exception:
            self.error = 'Невідома помилка, напишить нам що Ви робили на пошту!'
        return result


def main():
    a = MyCalculator()
    user_input = ''
    print('Калькулятор вміє робити наступні операції : +, -, /, *, ** (зведення в ступінь) '
          'та # (вилучення кореня, приклад: 25 # 2 = 5)')
    print('Якщо бажаєте вийти введіть exit або вийти\U0001F643')  # \U0001F643 = 🙃
    while True:
        user_input = input('Введіть Ваш вираз >>> ')
        if user_input.lower() in ('exit', 'вийти'):
            break
        elif user_input == '':
            continue
        a.calculate(user_input)


if __name__ == '__main__':
    main()
