f_lambda = lambda x: x.lower() == x[::-1].lower()


def main():
    inputdata = ('Країна', 'шалаш', 'Летел', 'вертольот', 'УЧУ', 'мем', 'мова')
    output_data = tuple(filter(f_lambda, inputdata))
    print(f'Початковий кортеж слів: {inputdata}\n'
          f'Вихідний кортеж: {output_data}')


if __name__ == '__main__':
    main()
