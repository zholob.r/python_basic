# ми не можемо використувати isinstance(x, int)
# тому що класс Bool являється підкласом Int
# isinstance(True, int)   # True
l_func = lambda x: str(x) if type(x) == int else x


def main():
    values = [1, 2, '3', 'forth', 'end', 99, True, None]
    new_values = list(map(l_func, values))
    print(f'Вхідні значення: {values}\n'
          f'Вихідні значення: {new_values}')


if __name__ == '__main__':
    main()
