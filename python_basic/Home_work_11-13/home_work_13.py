import random
from datetime import datetime

''' В цьому ДЗ вирішив порівняти швидкість сортування списку між стандартним методом list.sort()
 та швидкістю алгоритму сортування оптимизованого алгоритьму бульбашкою і для цікавості алгоритму сортування вибором 
 також довелось трохи (майже годину) помучатись і розібратись як робити генератор на функцію яка приймає 1 аргумент list'''


def time_decorator(func):
    def wraper(value):
        name = str(func)
        name = name[name.find(' ') + 1 : name.find('at') - 1]
        time = datetime.now()
        func(value)
        result = value
        print(f'Час виконання алгориму {name} на {len(value)} елементів: {datetime.now() - time}')
        return result

    return wraper


@time_decorator
def standart_sort(value: list):
    return value.sort()


''' Алгоритм buble_sort полягає в циклічних проходах по масиву, за кожен прохід елементи масиву попарно порівнюються 
і,якщо їх порядок неправильний, то здійснюється обмін. Обхід масиву повторюється до тих пір, 
поки масив не буде впорядкований.  Але після кожного проходу число елементів, що піддаються подальшому 
 порівнянню зменшується на 1 бо максимальне значення за кожен перехід гарантовано переміщєуться в кінець
 Та оптимізуємо тим, що введемо лічильник кількості обмінів, і якщо він == 0 то обміну не відбулось, 
 відповідно це значає що ліст отсортований і можно одразу завершити цикл, щоб не витрачати час на 
 зайві ітерації'''


@time_decorator
def buble_sort(value: list):
    for i in range(0, len(value)):
        count = 0
        for j in range(0, len(value) - i - 1):
            if value[j] > value[j + 1]:
                value[j], value[j + 1] = value[j + 1], value[j]
                count += 1
        if count == 0:
            break
    return value


'''Сортування вибором писок (або масив) ділиться на дві частини: список з відсортованими елементами та список з елементами,
 які потрібно відсортувати. Спочатку шукається найменший елемент у другому списку. Він додається в кінець першого. 
Таким чином алгоритм поступово формує список від меншого до більшого. 
Так відбувається до тих пір, поки не буде готовий відсортований масив.'''


@time_decorator
def selection_sort(value: list):
    for i in range(0, len(value)):
        minimum = i
        for j in range(i + 1, len(value)):
            if value[j] < value[minimum]:
                minimum = j
        value[minimum], value[i] = value[i], value[minimum]
    return value


def main():
    unsorted_list = [random.randrange(1, 101) for item in range(2000)]
    standart_sort(unsorted_list[:])
    buble_sort(unsorted_list[:])
    selection_sort(unsorted_list[:])
    print('-' * 100)

    unsorted_list = [random.randrange(1, 101) for item in range(10000)]
    standart_sort(unsorted_list[:])
    buble_sort(unsorted_list[:])
    selection_sort(unsorted_list[:])
    print('-' * 100)

    unsorted_list = [random.randrange(1, 101) for item in range(20000)]
    standart_sort(unsorted_list[:])
    buble_sort(unsorted_list[:])
    selection_sort(unsorted_list[:])


if __name__ == '__main__':
    main()
