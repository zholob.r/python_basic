from home_work_21 import Auto
import time


class Truck(Auto):
    def __init__(self, brand: str, age: int, mark: str, max_load: int, color='чорний', weight=1000):
        Auto.__init__(self, brand, age, mark, color, weight)
        self.max_load = max_load

    def move(self):
        print('attention')
        super().move()

    def load(self):
        time.sleep(1)
        print('load')
        time.sleep(1)


class Car(Auto):
    def __init__(self, brand: str, age: int, mark: str, max_speed: int, color='чорний', weight=1000):
        Auto.__init__(self, brand, age, mark, color, weight)
        self.max_speed = max_speed

    def move(self):
        super().move()
        print(f'max speed is {self.max_speed}')


truck_1 = Truck('бренд першого грузовика', 10, 'марка першого грузовика', 100, weight=10000)
print(f"""Бренд truck_1: {truck_1.brand} 
Вік truck_1: {truck_1.age} 
Марка truck_1: {truck_1.mark} 
max_load truck_1: {truck_1.max_load}
Колір truck_1: {truck_1.color}
Вага truck_1: {truck_1.weight}""")
truck_1.move()
truck_1.load()
truck_1.birthday()
truck_1.stop()
print('-' * 100)

truck_2 = Truck('бренд другого грузовика', 30, 'марка другого грузовика', 100, color='червоний')
print(f"""Бренд truck_2: {truck_2.brand} 
Вік truck_2 {truck_2.age} 
Марка truck_2: {truck_2.mark} 
max_load truck_2: {truck_2.max_load}
Колір truck_2: {truck_2.color}
Вага truck_2: {truck_2.weight}""")
truck_2.move()
truck_2.load()
truck_2.birthday()
truck_2.stop()
print('-' * 100)

car_1 = Car('бренд першої машини', 30, 'марка першої машиниа', 250)
print(f"""Бренд car_1: {car_1.brand} 
Вік car_1 {car_1.age} 
Марка car_1: {car_1.mark} 
max_speed car_1: {car_1.max_speed}
Колір car_1: {car_1.color}
Вага car_1: {car_1.weight}""")
car_1.move()
car_1.birthday()
car_1.stop()
print('-' * 100)

car_2 = Car('бренд другої машини', 30, 'марка другої машиниа', 350, color='жовта')
print(f"""Бренд car_2: {car_2.brand} 
Вік car_2 {car_2.age} 
Марка car_2: {car_2.mark} 
max_speed car_2: {car_2.max_speed}
Колір car_2: {car_2.color}
Вага car_2: {car_2.weight}""")
car_2.move()
car_2.birthday()
car_2.stop()
print('-' * 100)
