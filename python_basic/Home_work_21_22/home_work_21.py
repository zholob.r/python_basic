class Auto:
    brand = None
    age = None
    color = None
    mark = None
    weight = None

    def __init__(self, brand: str, age: int, mark: str, color='чорний', weight=1000):
        Auto.brand = brand
        Auto.age = age
        Auto.mark = mark
        Auto.color = color
        Auto.weight = weight

    def move(self):
        print('move')

    def birthday(self):
        self.age += 1

    def stop(self):
        print('stop')
