while (True):
    name = input('Введіть Ваше ім\'я >>> ')
    age = input('Введіть Ваш вік >>> ')
    if age == '0' or not age.isdigit():
        print('Помилка, повторіть введення!')
        continue
    age = int(age)
    if age < 10:
        print(f'Привіт, шкет {name}')
    elif age <= 18:
        print(f'Як справи, {name}?')
    elif age < 100:
        print(f'Що бажаєте {name}?')
    else:
        print(f'{name}, Ви брешете - у нас стільки не живуть...')

    end = input('Бажаєте вийти? (Д/Y) >>> ')
    if end.upper() == 'Д' or end.upper() == 'Y':
        break
