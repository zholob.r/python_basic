def number_recognition(str_number: str) -> str:
    new_str_number = str_number.replace(',', '.')  # якщо є коми одразу міняємо на '.'
    can_be_number = True
    # ітеруємо кожен символ строки і якщо це буква,
    # або будь-який символ окрім числа та '.' і '-'  то не може бути числом
    for symbol in new_str_number:
        if symbol.isalpha() or (symbol not in ('.', '-') and not symbol.isdigit()):
            can_be_number = False
    description = 'від\'ємне ' if new_str_number[0] == '-' else 'позитивне '
    output_str = ''

    # якщо НЕ може бути числом, або містить більше 2х '.', або '-' не перший символ  то ввід неправильний ! !
    if not can_be_number or new_str_number.count('.') >= 2 or new_str_number == '.' or new_str_number[1:].count('-') >= 1:
        description = 'неправильне'
        # якщо ми замінили всі ',' на '.' і неправильний ввід в котрому фігурує кома, то треба її повернути назад
        new_str_number = str_number
    elif new_str_number.count('.') == 1:
        description += 'дробове'
        new_str_number = str(float(new_str_number))  # для того, щоб нормально виводило .5 як 0.5
    else:
        description += 'ціле'

    if new_str_number == '0':
        output_str = 'Ви ввели нуль'
    else:
        output_str = f'Ви ввели {description} число: {new_str_number}'

    return output_str


def main():
    print('Вийти з програми можна ввевши "вихід", "exit", "quit", "e" або "q" \U0001F643')  # 🙃
    while True:
        input_str = input('Введіть число >> ')
        if input_str.lower() in ('вихід', 'exit', 'quit', 'e', 'q'):
            break
        elif input_str == '':
            continue
        print(number_recognition(input_str))


if __name__ == '__main__':
    main()


#рішення викладача
# def func(val):
#     ret = "Вы ввели "
#     val = val.replace(",", ".").strip()
#     if not val.replace("-", "", 1).replace(".", "", 1).strip().isdigit():
#         ret += " не правильне число " + val
#     elif val == "0" or (val.replace("-", "", 1).replace(".", "", 1).isdigit()
#                         and int(val.replace("-", "", 1).replace(".", "", 1)) == 0):
#         ret += "нуль"
#     else:
#         ret += "від'ємне " if "-" in val else "позитивне "
#         ret += "ціле число: " if "." not in val else "дробове число: "
#         ret += val
#     return ret