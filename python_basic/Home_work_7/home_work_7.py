# шаблон генератора словника     dict_variable = {key:value for (key,value) in dictonary.items()}


chess_players = {"Carlsen, Magnus": 1865,
                 "Firouzja, Alireza": 2804,
                 "Ding, Liren": 2799,
                 "Caruana, Fabiano": 1792,
                 "Nepomniachtchi, Ian": 2773
                 }

new_chess_players = {value: key for (key, value) in chess_players.items() if value >= 2000}

# беру зріз з 1го символу по ',' не включаючи її -> потім зріз з ' ' і наступний елемент + '.'
new_chess_players = {key: value[:value.find(',')] + value[value.find(' '): value.find(' ') + 2] + '.' for (key, value) in new_chess_players.items()}

print(f'chess_players: {chess_players} \n'                 
      f'new_chess_players: {new_chess_players}')    # {2804: 'Firouzja A.', 2799: 'Ding L.', 2773: 'Nepomniachtchi I.'}
