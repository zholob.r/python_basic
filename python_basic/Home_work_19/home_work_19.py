from random import random, randrange, choice
import json
import csv


def create_data():
    operator_code = ['095', '066', '098', '096', '050', '097']
    with open('../Home_work_18/data.json', encoding='utf-8') as file:
        input_data = json.load(file)
    data = [['ID', 'Ім\'я', 'Вік', 'Телефон']]
    for key in input_data:
        temp = []
        temp.append(int(key))       # чомусь завелике (6-значне) число в .json записало як строку
        temp.extend(input_data[key])
        if random() > 0.25:  # якщо ймовірність більше 25% то номер є і додаємо його, інакше нічого не додаємо
            temp.append(choice(operator_code) + str(randrange(1000000, 10000000)))
        else:
            temp.append('')
        data.append(temp)
    return data


def main():
    data = create_data()
    with open('data.csv', 'w', newline='', encoding='utf-8') as file:
        file_writer = csv.writer(file)
        file_writer.writerows(data)


if __name__ == '__main__':
    main()
