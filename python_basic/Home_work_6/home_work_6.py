number = input('Введіть ціле додатне число >>> ')

user_number = int(number) if (number.isdigit() and int(number) > 0) else 0

sum = 0
number = user_number
while number > 0:
    if number % 3 != 0:
        sum += number ** 3
    number -= 1
print(f'Сума кубів від 0 до Вашого числа окрім тих, що кратні 3 = {sum} (відпрацював while)')

sum = 0
for item in range(1, user_number + 1):
    if item % 3 != 0:
        sum += item ** 3
print(f'Сума кубів від 0 до Вашого числа окрім тих, що кратні 3 = {sum} (відпрацював for)')
