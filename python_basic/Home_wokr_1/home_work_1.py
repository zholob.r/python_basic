user_str = input('Введіть Ваш текст >>>')

out_pair_str = user_str[1::2]
out_upper_str = user_str[::-1].upper()

print(f'Ваш текст: {user_str} \n'
      f'Строка з кожним парним символом Вашого тексту: {out_pair_str} \n'
      f'Ваша строка в зворотньому порядку написана дуже нервовою людиною: {out_upper_str}')
