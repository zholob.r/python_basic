import random


# загадуємо число
def create_number():
    return random.randrange(1, 11)


#
def convert_numeric(user_input: str):
    '''
    перевірка що ввів користувач і якщо число яке нам підходить то конвертуємо в інт
    :return:    -1 - значить хоче вийти з гри в довільний момент і ввів QUIT
                false - ввів текст або число не в межах діапазону 1-10, і користувачу про це повідомлять
                number - якщо все добре поверне введене число в форматі int
    '''
    # якщо хоче завершити гру
    if user_input.strip().upper() == 'QUIT':
        return -1
    if user_input.strip().isdigit():
        number = int(user_input)
        if number > 10:
            print('Ви ввели число більше 10 !')
            return False
        elif number < 1:
            print('Ви ввели число менше 1 !')
            return False
        else:
            return number
    else:
        print('Ви ввели не число! ')
        return False


def game(number):
    continue_game = 'Y'
    count = 2
    print('{:^60}\n'
          '{:^60}\n'
          '{:^60}'.format('Ласкаво просимо в гру Вгадай число!', 'У Вас є 3 спроби вгадати число від 1 до 10',
                          '(якщо Вам набридло то вийти можна ввевши quit)'))

    while continue_game.upper() in {'Y', 'Д'}:
        user_number = input('Введіть число від 1 до 10 >>> ')
        user_number = convert_numeric(user_number)
        if not user_number:
            continue
        elif user_number == -1:
            break
        if user_number == number:
            print('{:^60}'.format('Вітаю! ви вгадали число!!!'))
            continue_game = input('Бажаєте зіграти ще? (Д / Y) >>> ')
            count = 2
        else:
            if count > 0:
                print('{:>60} {}'.format('Залишилось спроб: ', count))
                count -= 1
            else:
                print(f'Нажаль, Ви програли! Було задумано число {number}')
                count = 2
                continue_game = input('Бажаєте зіграти ще? (Д / Y) >>> ')


def main():
    game(create_number())


main()
