even_odd = ''
number = input('Введіть ціле число >>> ')
if float(number) % 1 != 0:
    print('Не вірне введення')
else:
    number = int(number)
    even_odd = 'непарне число' if number % 2 else 'нуль' if number == 0 else 'парне число'
    print(f'Ви ввели {even_odd}')
