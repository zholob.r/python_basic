def main():
    line_1, line_2, line_3, line_4 = [input(f'Введть {i + 1} строку >> ') + '\n' for i in range(4)]
    file = open('HW17.txt', 'w', encoding='utf-8')
    file.write(line_1)
    file.write(line_2)
    file.close()
    with open('HW17.txt', 'a', encoding='utf-8') as file:
        file.write(line_3)
        file.write(line_4)


if __name__ == '__main__':
    main()
