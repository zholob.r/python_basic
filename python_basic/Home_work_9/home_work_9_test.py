import random
from datetime import datetime

''' Трохи подумавши я не побачив сенсу перебирати кожен елемент списку чисел і 
збільшувати значення ключа на 1 тим самим отримуючи кількість входжень чисел.
Замість цього я зрозумів, шо легше герератором словників зробити словник з ключами, 
які дорівнюють числам які входять до списку і значенями за замовчуванням 0 
Наприклад: amount_numbers = {0: 0, 1: 0, 2: 0, і тощо}
Як отримати числа які входять до списку? Все просто - привести список до множини, 
адже вона не може мати дублів. І далі просто ми через метод .count() підраховуєм 
скільки разів входить до списку те число, яке міститься  ключі нашого словаря 
 '''


def count_numbers(numbers: list) -> dict:
    # заповнили початковий словник кдічами  які є
    amount_numbers = {key: 0 for key in set(numbers)}
    for key in amount_numbers:
        amount_numbers[key] = numbers.count(key)
    return amount_numbers


'''просто ітеруємо список, і і додаємо потрібні ключі якщо неообхідно 
 і збільшуємо кількість входжень того чи іншого числа в знченні відповідного ключа 
 '''


def count_numbers_iterating(numbers: list) -> dict:
    amount_numbers = {}
    for number in numbers:
        if number in amount_numbers.keys():
            amount_numbers[number] = amount_numbers[number] + 1
        else:
            amount_numbers[number] = 1
    return amount_numbers


''' тут ми проходимось кожного разу беремо 1й елемент списку, дізнаємось скільки разів він входить 
і видяляємо всі ці елементи, тим самим нам не доводиться заново проходити по елементам які вже підраховані
але для цього нам спершу треба відсортувати список'''
def count_numbers_iterating_deleting(numbers: list) -> dict:
    amount_numbers = {}
    numbers.sort()
    while len(numbers) != 0:
        item = numbers[0]
        amount_numbers[item] = numbers.count(item)
        del numbers[:numbers.count((item))]
    return amount_numbers


def main():
    list_nubmers = [random.randrange(1, 11) for i in range(200)]

    # якщо 200 елементів 1 - 10
    now = datetime.now()
    count_numbers(list_nubmers)
    print('list_nubmers() на 200 елементів 1 - 10: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating(list_nubmers)
    print('count_numbers_iterating() на 200 елементів 1 - 10: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating_deleting(list_nubmers[:])
    print('count_numbers_iterating_deleting() на 200 елементів 1 - 10: ', datetime.now() - now)
    print('-' * 100)

    # якщо 2000 елементів 1 - 10
    list_nubmers = [random.randrange(1, 11) for i in range(2000)]
    now = datetime.now()
    count_numbers(list_nubmers)
    print('list_nubmers() на 2000 елементів 1 - 10: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating(list_nubmers)
    print('count_numbers_iterating() на 2000 елементів 1 - 10: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating_deleting(list_nubmers[:])
    print('count_numbers_iterating_deleting() на 2000 елементів 1 - 10: ', datetime.now() - now)
    print('-' * 100)

    # якщо 2000 елементів 1 - 100
    list_nubmers = [random.randrange(1, 101) for i in range(2000)]
    now = datetime.now()
    count_numbers(list_nubmers)
    print('list_nubmers() на 2000 елементів 1 - 100: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating(list_nubmers)
    print('count_numbers_iterating() на 2000 елементів 1 - 100: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating_deleting(list_nubmers[:])
    print('count_numbers_iterating_deleting() на 2000 елементів 1 - 100: ', datetime.now() - now)
    print('-' * 100)

    # якщо 20000 елементів 1 - 100
    list_nubmers = [random.randrange(1, 101) for i in range(20000)]
    now = datetime.now()
    count_numbers(list_nubmers)
    print('list_nubmers() на 20000 елементів 1 - 100: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating(list_nubmers)
    print('count_numbers_iterating() на 20000 елементів 1 - 100: ', datetime.now() - now)
    now = datetime.now()
    count_numbers_iterating_deleting(list_nubmers[:])
    print('count_numbers_iterating_deleting() на 20000 елементів 1 - 100: ', datetime.now() - now)


if __name__ == '__main__':
    main()
