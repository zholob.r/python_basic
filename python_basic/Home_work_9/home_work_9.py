import random

''' Трохи подумавши я не побачив сенсу перебирати кожен елемент списку чисел і 
збільшувати значення ключа на 1 тим самим отримуючи кількість входжень чисел.
Замість цього я зрозумів, шо легше герератором словників зробити словник з ключами, 
які дорівнюють числам які входять до списку і значенями за замовчуванням 0 
Наприклад: amount_numbers = {0: 0, 1: 0, 2: 0, і тощо}
Як отримати числа які входять до списку? Все просто - привести список до множини, 
адже вона не може мати дублів. І далі просто ми через метод .count() підраховуєм 
скільки разів входить до списку те число, яке міститься  ключі нашого словаря 
Провівши тести в home_work_9_test.py цей алгоритм чудово показав себе на словнику 
в 2000 елементів зі значенями 1 - 10, але простий перебор словника з 20 000 елементів
зі значенями 1 - 100 відпрацював в 5 разів швидше ніж цей алгоритм і швидше ніж список 
з 2000 елементів, які мають значення 1 - 100
 '''


def count_numbers(numbers: list) -> dict:
    amount_numbers = {key: 0 for key in set(numbers)}
    for key in amount_numbers:
        amount_numbers[key] = numbers.count(key)
    return amount_numbers


def sort_dict(input_dict: dict) -> dict:
    # погугливши зрозумів що нема іншого виходу відсортувати словик окрім як
    # сортувати ключі методом лісту .sort()
    temp = list(input_dict.keys())
    temp.sort()
    output_dict = {key: input_dict[key] for key in temp}
    return output_dict


def output(input_d: dict) -> str:
    end = lambda x: 'раз' if x % 10 in (0, 1) else 'разів' if 11 <= x % 100 <= 20 or 5 <= x % 10 <= 9 else 'раза'
    result = ''
    for key in input_d:
        result += f'Число {key} зустрічається у первісному списку {input_d[key]} {end(input_d[key])}\n'
    return result


def main():
    # наш список з 200 елементів заповнений випадковим числом від 1 до 10
    list_nubmers = [random.randrange(1, 11) for i in range(200)]
    count_dict = count_numbers(list_nubmers)
    count_dict = sort_dict(count_dict)
    print(f'Початковий список: {list_nubmers} \n'
          f'Словник в якому пораховано кільки разів трапляється кожне число {count_dict} \n'
          f'{output(count_dict)}')


if __name__ == '__main__':
    main()
