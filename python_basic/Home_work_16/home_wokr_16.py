def main():
    code = b'r\xc3\xa9sum\xc3\xa9'
    print(f'Закодований рядок в форматі utf-8: {code}')
    decode = code.decode()
    print(f'Декодований рядок в форматі utf-8: {decode}')
    code = decode.encode('latin1')
    print(f'Закодований рядок в форматі Latin-1: {code}')
    print(f'Декодований рядок в форматі Latin-1: {code.decode("latin1")}')


if __name__ == '__main__':
    main()
